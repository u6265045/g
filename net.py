"""Defines the neural network, losss function and metrics"""

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import sampler as distribution_sampler


class Net(nn.Module):
    """
    This is the standard way to define your own network in PyTorch.
    """

    def __init__(self, config, input_dim, output_dim):
        """
        We define a multilayer perceptron network as our g function. The components
        required are:
        Args:
            config: (config) contains:
              - softmax_in: a boolean. When True, applies softmax on the inputs
              - num_hiddens: a list with each element corresponding number of hidden unit for
                the respective hidden layer.
              - residual: a boolean. When True, it will add the input to the output of the MLP.
                          only works when input_dim is equal to the output_dim.
              - batch_norm: a boolean. When True, uses batch normalization after each FC layer.
              - activation: a string representing the activation function for the network.
                it can be 'relu', 'sigmoid', or 'tanh' currently.
            input_dim: dimensionality of the input.
            output_dim: dimensionality of the output.
        """
        super(Net, self).__init__()
        self._config = config
        self._residual = config.residual
        self._softmax_in = config.softmax_in
        self._output_dim = output_dim
        self._input_dim = input_dim
        last_hidden = input_dim
        g_layers = []
        activation_dict = {'relu': nn.ReLU,
                           'sigmoid': nn.Sigmoid,
                           'tanh': nn.Tanh,
                           'softplus': nn.Softplus}
        assert config.activation in activation_dict, 'activation {} not supported'.format(config.activation)
        if self._residual is True:
          assert input_dim == output_dim
        for num_hidden in config.num_hiddens:
          if config.batch_norm:
            g_layers.append(nn.BatchNorm1d(last_hidden))
          g_layers.append(nn.Linear(last_hidden, num_hidden, bias=True))
          g_layers.append(activation_dict[config.activation]())
          last_hidden = num_hidden
        self.g_layers = nn.Sequential(*g_layers)
        if config.batch_norm:
          self.bn = nn.BatchNorm1d(last_hidden)
        else:
          self.bn = lambda x:x
        self.fc = nn.Linear(last_hidden, output_dim)

    def forward(self, x):
        """
        This function defines how we use the components of our network to operate on an input batch.

        Args:
            x: (Variable) contains a batch of inputs, of dimension batch_size x output_dim.

        Returns:
            out: (Variable) dimension batch_size x output_dim with the log probabilities for the labels of each input.
        """
        if self._softmax_in:
          x = F.softmax(x, dim=1)
        out = self.g_layers(x)
        out = self.fc(self.bn(out))
        if self._residual:
          out = out + x
        return out

class SkipMLPNet(nn.Module):
    """
    This is MLP with residual on hidden units.
    """

    def __init__(self, config, input_dim, output_dim):
        """
        We define a multilayer perceptron network as our g function with residuals.
        The components required are:
        Args:
            config: (config) contains:
              - softmax_in: a boolean. When True, applies softmax on the inputs
              - num_hiddens: a list with each element corresponding number of hidden unit for
                the respective hidden layer in the encoder. The decoder has reverse layers
                except the last one in the input list.
                e.g., if it is [500,200,100] the network will be [500,200,100,200,500] with
                skip connections between layers with 500,200 hidden units.
              - residual: a boolean. When True, it will add the input to the output of the MLP.
                          only works when input_dim is equal to the output_dim.
              - batch_norm: a boolean. When True, uses batch normalization after each FC layer.
              - activation: a string representing the activation function for the network.
                it can be 'relu', 'sigmoid', or 'tanh' currently.
            input_dim: dimensionality of the input.
            output_dim: dimensionality of the output.
        """
        super(SkipMLPNet, self).__init__()
        self._config = config
        self._residual = config.residual
        self._softmax_in = config.softmax_in
        self._output_dim = output_dim
        self._input_dim = input_dim
        last_hidden = input_dim
        g_layers = []
        self.activation_dict = {'relu': F.relu,
                                'sigmoid': F.sigmoid,
                                'tanh': F.tanh,
                                'softplus': F.softplus}
        assert config.activation in self.activation_dict, 'activation {} ' \
                                                          'not supported'.format(config.activation)
        if self._residual is True:
          assert input_dim == output_dim
        assert len(config.num_hiddens) > 1, 'num_hiddens should be more than one'
        self._num_hiddens = config.num_hiddens + config.num_hiddens[:-1][::-1]
        if config.batch_norm:
          self._batch_norms = []
        self._linear_layers = []
        for num_hidden in self._num_hiddens:
          if config.batch_norm:
            self._batch_norms.append(nn.BatchNorm1d(last_hidden))
          self._linear_layers.append(nn.Linear(last_hidden, num_hidden, bias=True))
          last_hidden = num_hidden
        self._linear_layers = nn.ModuleList(self._linear_layers)
        if config.batch_norm:
          self._batch_norms = nn.ModuleList(self._batch_norms)
          self.bn = nn.BatchNorm1d(last_hidden)
        else:
          self.bn = lambda x:x
        self.fc = nn.Linear(last_hidden, output_dim)

    def forward(self, x):
        """
        This function defines how we use the components of our network to operate on an input batch.

        Args:
            x: (Variable) contains a batch of inputs, of dimension batch_size x output_dim.

        Returns:
            out: (Variable) dimension batch_size x output_dim with the log probabilities for the labels of each input.
        """
        preact = True
        if 'preact' in self._config:
          preact = self._config.preact #should be boolean
        if self._softmax_in:
          x = F.softmax(x, dim=1)
        outs = []
        half_way = len(self._num_hiddens)//2
        last_out = x
        for i in range(half_way+1):
          if self._config.batch_norm:
            last_out = self._batch_norms[i](last_out)
          last_out = self._linear_layers[i](last_out)
          #Note add residual before activation!
          if preact:
            outs.append(last_out)
          last_out = self.activation_dict[self._config.activation](last_out)
          if not preact:
            outs.append(last_out)
        outs = outs[:-1][::-1]
        for i in range(half_way):
          j = i + half_way + 1
          if self._config.batch_norm:
            last_out = self._batch_norms[j](last_out)
          last_out = self._linear_layers[j](last_out)
          if preact:
            last_out = outs[i] + last_out
          last_out = self.activation_dict[self._config.activation](last_out)
          if not preact:
            last_out = outs[i] + last_out

        out = self.fc(self.bn(last_out))
        if self._residual:
          out = out + x
        return out

def softmax_cross_entropy(logits, labels):
    """
    Compute the cross entropy loss given outputs and labels.

    Args:
        outputs: (Variable) dimension batch_size x C - output of the model
        labels: (Variable) dimension batch_size, where each element is a value in [0, C-1]

    Returns:
        loss (Variable): cross entropy loss for all inputs in the batch

    Note: you may use a standard loss function from http://pytorch.org/docs/master/nn.html#loss-functions. This example
          demonstrates how you can easily define a custom loss function.
    """
    loss =  nn.CrossEntropyLoss()
    return loss(logits, labels)

class DistributionLoss(object):
  def __init__(self, sampler):
    super(DistributionLoss, self).__init__()
    self.sampler = sampler

  def loss(self, logits, labels):
    '''
    logits: [batch, output_dim] tensor, as output of g
    labels: [batch] true labels
    '''
    # [batch * num_classes] --TODO: sampler only works on cpu
    log_posterior = self.sampler.log_posterior(logits.cpu()).cuda()
    batch = log_posterior.shape[0]
    loss = -log_posterior[range(batch), labels].mean()
    return loss

def get_loss_fn(config):
  if not 'output_distribution' in config:
    return softmax_cross_entropy
  distribution_loss = DistributionLoss(config.output_distribution)
  return distribution_loss.loss

def accuracy(outputs, labels):
    """
    Compute the accuracy, given the outputs and labels for all images.

    Args:
        scores: (np.ndarray) dimension batch_size x C -  output of the model
        labels: (np.ndarray) dimension batch_size, where each element is a value in [0, C-1]

    Returns: (float) accuracy in [0,1]
    """
    outputs = np.argmax(outputs, axis=1)
    return np.sum(outputs==labels[:,0])/float(labels.size)

class DistributionAccuracy(object):
  def __init__(self, sampler):
    super(DistributionAccuracy, self).__init__()
    self.sampler = sampler

  def accuracy(self, logits, labels):
    '''
    logits: [batch, output_dim] numpy array, as output of g
    labels: [batch] true labels numpy array
    '''
    # [batch * num_classes]
    log_posterior = self.sampler.log_posterior(torch.tensor(logits)).data.cpu().numpy()
    return accuracy(log_posterior, labels)

def get_metrics_fn(config):
  if not 'output_distribution' in config:
    return {'accuracy': accuracy}
  dist_acc = DistributionAccuracy(config.output_distribution)
  return {'accuracy': dist_acc.accuracy}


def get_model(config):
  net_fn = Net
  if 'type' in config.net:
    if config.net.type == 'skip_mlp':
      net_fn = SkipMLPNet
    elif config.net.type == 'mlp':
      net_fn = Net
    else:
      raise NotImplementedError('Network type is not implemented')
  # Define the model and optimizer
  if 'output_sampler' not in config or config.output_sampler is None:
    model = net_fn(config.net, config.dim, config.num_classes)
  else:
    out_sampler = distribution_sampler.get_sampler(config, config.output_sampler)
    config.output_distribution = out_sampler
    #model should be from dim to dim
    model = net_fn(config.net, config.dim, config.dim)
  return model
