import json
from copy import deepcopy
import itertools
import os

def write_config(path,
    dim = 10,
    num_classes = 10,
    locs_scale = 1,
    sampler = "random_mixture",
    output_sampler = None,
    seed = 1357,
    min_num_components = 2,
    max_num_components = 5,
    cov_type = "identity",
    num_eval = 10, #number of eval batches
    train_weight_decay = 0,
    train_num_epochs = 100,
    train_batch_size = 1024,
    train_num_workers = 1,
    train_save_summary_steps = 10,
    train_learning_rate = 0.001,
    net_softmax_in = False,
    net_num_hiddens = [100,100],
    net_residual = False,
    net_batch_norm = False,
    net_activation = "relu",
    net_type = "mlp",
    ):
  base_net = {
    "softmax_in": net_softmax_in,
    "num_hiddens": net_num_hiddens,
    "residual": net_residual,
    "batch_norm": net_batch_norm,
    "activation": net_activation,
    "type": net_type,
  }

  base_train = {
    "weight_decay": train_weight_decay,
    "num_epochs": train_num_epochs,
    "batch_size": train_batch_size,
    "num_workers": train_num_workers,
    "save_summary_steps": train_save_summary_steps,
    "learning_rate": train_learning_rate,
  }

  base_config = {
    "sampler": sampler,
    "dim": dim,
    "num_classes": num_classes,
    "locs_scale": locs_scale,
    "output_sampler": output_sampler,
    "seed": seed,
    "min_num_components": min_num_components,
    "max_num_components": max_num_components,
    "cov_type": cov_type,
    "num_eval": num_eval,
    "train": base_train,
    "net": base_net,
  }

  with open(path, 'w') as f:
    json.dump(base_config, f, indent=2,separators=(',',': '))


def get_num_params(num_hiddens, in_dim, out_dim):
  num_units = [in_dim] + num_hiddens + [out_dim]
  return sum([num_units[i]*num_units[i+1] for i in range(len(num_units)-1)])

def gen_hidden_combinations(num_hidden_list, max_layers,
                            in_dim, out_dim, max_params):
  all_hiddens = []
  for i in range(max_layers):
    all_hiddens += list(itertools.product(*[num_hidden_list]*(i+1)))
  all_hiddens = [list(x) for x in all_hiddens]
  all_hiddens = [num_hiddens for num_hiddens in all_hiddens \
                 if get_num_params(num_hiddens, in_dim, out_dim) <= max_params]
  return all_hiddens

def hiddens2str(h):
  s = str(h[0])
  for i in range(1,len(h)):
    s = s + '_' + str(h[i])
  return s

def generate1(base_dir):
  base_params = dict(
    dim = 10,
    num_classes = 10,
    locs_scale = 1,
    sampler = "random_mixture",
    output_sampler = None,
    seed = 1357,
    min_num_components = 2,
    max_num_components = 5,
    cov_type = "random",
    num_eval = 10,
    train_weight_decay = 0,
    train_num_epochs = 100,
    train_batch_size = 1024,
    train_num_workers = 1,
    train_save_summary_steps = 10,
    train_learning_rate = 0.001,
    net_softmax_in = False,
    net_num_hiddens = [100,100],
    net_residual = False,
    net_batch_norm = False,
    net_activation = "relu",
    net_type = "mlp",
  )

  model_dirs = []

  dims = [10,100] #num_classes is induced by dim (they are equal)
  #cov_types = ['identity', 'random']
  train_learning_rates = [0.01, 0.005]
  net_activations = ["relu", "tanh"]
  locs_scales = [0.1, 0.5, 1.0]
  for (dim, lr, net_activation, locs_scale) in itertools.product(dims,
                                                                 train_learning_rates,
                                                                 net_activations,
                                                                 locs_scales):
    num_classes = dim
    if dim == 10:
      all_hiddens = gen_hidden_combinations([50,100, 150,200], 3,
                                            dim, dim, 10000)
    else:
      all_hiddens = gen_hidden_combinations([50,100,200,300], 3,
                                            dim, dim, 40000)
    for net_num_hiddens in all_hiddens:
      params = deepcopy(base_params)
      params['dim'] = dim
      params['train_learning_rate'] = lr
      params['num_classes'] = num_classes
      params['net_num_hiddens'] = net_num_hiddens
      params['net_activation'] = net_activation
      params['locs_scale'] = locs_scale
      exp_name = 'dim_{}/locs_scale_{}/lr_{}/{}/hidden_{}'.format(dim, locs_scale,
                                                              lr, net_activation,
                                                              hiddens2str(net_num_hiddens))
      exp_root = os.path.join(base_dir, exp_name)
      if not os.path.exists(exp_root):
        os.makedirs(exp_root)
      config_name = os.path.join(exp_root, 'config.json')
      write_config(config_name, **params)
      model_dirs.append(exp_root)
      print('- {} added'.format(exp_root))

  return model_dirs


def generate2(base_dir):
  base_params = dict(
    dim = 10,
    num_classes = 10,
    locs_scale = 1,
    sampler = "random_mixture",
    output_sampler = None,
    seed = 1357,
    min_num_components = 2,
    max_num_components = 5,
    cov_type = "random",
    num_eval = 10,
    train_weight_decay = 0,
    train_num_epochs = 20, #This is for getting the best settings
    train_batch_size = 1024,
    train_num_workers = 1,
    train_save_summary_steps = 10,
    train_learning_rate = 0.001,
    net_softmax_in = False,
    net_num_hiddens = [100,100],
    net_residual = False,
    net_batch_norm = False,
    net_activation = "relu",
    net_type = "mlp",
  )

  model_dirs = []

  dims = [10] #[10,100] #num_classes is induced by dim (they are equal)
  #cov_types = ['identity', 'random']
  train_learning_rates = [0.0001,0.0005] #[0.0005, 0.0001] #[0.01, 0.005,0.001]
  net_activations = ["relu", "tanh", "softplus"]
  locs_scales = [0.1, 0.5, 1.0] # [0.1, 1.0]
  for (dim, lr, net_activation, locs_scale) in itertools.product(dims,
                                                                 train_learning_rates,
                                                                 net_activations,
                                                                 locs_scales):
    num_classes = dim
    if dim == 10:
      all_hiddens = gen_hidden_combinations([50,100, 150,200], 3,
                                            dim, dim, 10000)
    else:
      all_hiddens = gen_hidden_combinations([100,200], 3, #[50,100,200,300]
                                            dim, dim, 40000)
    for net_num_hiddens in all_hiddens:
      params = deepcopy(base_params)
      params['dim'] = dim
      params['train_learning_rate'] = lr
      params['num_classes'] = num_classes
      params['net_num_hiddens'] = net_num_hiddens
      params['net_activation'] = net_activation
      params['locs_scale'] = locs_scale
      exp_name = 'dim_{}/locs_scale_{}/lr_{}/{}/hidden_{}'.format(dim, locs_scale,
                                                              lr, net_activation,
                                                              hiddens2str(net_num_hiddens))
      exp_root = os.path.join(base_dir, exp_name)
      if not os.path.exists(exp_root):
        os.makedirs(exp_root)
      config_name = os.path.join(exp_root, 'config.json')
      write_config(config_name, **params)
      model_dirs.append(exp_root)
      print('- {} added'.format(exp_root))

  return model_dirs

def split(a, n):
  k, m = divmod(len(a), n)
  return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))


def main(exp_name):

  out = '{}_{}.sh'
  num_files = 8
  all_model_dirs = generate2('experiments/{}'.format(exp_name))
  model_dirs_splits = split(all_model_dirs, num_files)
  for i, model_dirs in enumerate(model_dirs_splits):
    with open(out.format(exp_name,i),'w') as f:
      f.write('#!/usr/bin/env bash\n')
      f.write('export CUDA_VISIBLE_DEVICES=0\n\n')
      for model_dir in model_dirs:
        f.write('python train.py --model_dir {}\n'.format(model_dir))

if __name__ == '__main__':
  main('combinations_gen5')
