import torch
from torch.utils import data
import os, sys
import numpy as np
from torch.distributions.multivariate_normal import MultivariateNormal
import sampler as data_sampler

class SamplerDataset(data.Dataset):
  def __init__(self, config, sampler):
    '''
    A pytorch dataset
    Args:
      config: configuration object
      sampler: sampler object which we can sample data from
    '''
    super(SamplerDataset, self).__init__()
    self.sampler = sampler
    self.config = config

  def __len__(self):
    return 100000

  def __getitem__(self, index):
    return self.sampler.sample()

def fetch_dataloader(config):
  sampler = data_sampler.get_sampler(config, config.sampler)
  dl = data.DataLoader(
                       SamplerDataset(config, sampler),
                       batch_size=config.train.batch_size,
                       shuffle=None,
                       num_workers=config.train.num_workers,
                       pin_memory=config.cuda)
  return dl, sampler


