import torch
from torch.utils import data
import os, sys
import numpy as np
from torch.distributions.multivariate_normal import MultivariateNormal
import pyro
import pyro.distributions as dist

class Sampler(object):
  """Super class for all of the samplers
  """
  def __init__(self):
    pass

  def sample(self):
    return None

  def log_posterior(self, x):
    return None

  def posterior(self,x):
    return torch.exp(self.log_posterior(x))

  def log_prob(self, x):
    return None

  def prob(self,x):
    return torch.exp(self.log_prob(x))

class MultivariateNormalSampler(Sampler):
  def __init__(self, config, locs, covs=None, priors=None):
    '''
    Generates data from a multivariate normal distribution for n classes
      with locs as the mean, cov as the covariances and priors as priors
      for each class.
    Args:
      config: configuration object
      locs: [n,m] numpy array with each row (i.e., locs[r]) representing
            the mean for the r-th class and m is the dimensionality.
      covs: [n,m,m] numpy array of covariances where covs[r] is the covariance
            matrix for the r-th class.
            If convs is None, assumes identity covariances.
      priors: [n,] numpy array which priors[r] is used as the relative
              priors for r-th class.
              If priors is None, assumes uniform priors.
              priors should sum to one.

    '''
    super(MultivariateNormalSampler, self).__init__()
    self.locs = locs
    self.num_classes = locs.shape[0]
    self.dim = locs.shape[1]
    if covs is None:
      covs = torch.tensor(np.tile(
                                  np.eye(self.dim, dtype=np.float32)[None],
                                  [self.num_classes, 1, 1]))
    self.covs = covs
    if priors is None:
      priors = torch.tensor(np.array([1./self.num_classes]*self.num_classes,
                            dtype=np.float32))
    self.priors = priors
    self.log_priors = torch.log(self.priors)
    self.config = config
    self._check_shapes(config, locs, covs, priors)
    self.mv_normal_samplers = []
    for loc,cov in zip(self.locs, self.covs):
      try:
       self.mv_normal_samplers.append(
          MultivariateNormal(loc, cov))
      except:
        from IPython import embed;embed()

  def _check_shapes(self, config, locs, covs, priors):
    assert len(locs.shape) == 2, 'locs should be a 2-dimensional numpy array'
    assert len(covs.shape) == 3, 'covs should be a 3-dimensional numpy array'
    assert covs.shape[1] == covs.shape[2], 'covs should be (n,m,m)'
    assert len(priors.shape) == 1, 'prior should be a 1-dimensional numpy array'
    assert priors.shape[0] == locs.shape[0] == covs.shape[0], 'shape mismatch'
    #should be carefull about float32, float64 (priors.sum() == 1 does not work)
    assert np.allclose(priors.data.numpy().sum(), 1), 'priors should sum to 1'

  def sample(self):
    k =  torch.multinomial(self.priors, 1)
    x = self.mv_normal_samplers[int(k[0])].sample()
    return x,k

  def log_posterior(self, x):
    '''
    Returns the log probability of a sample x for each class,
      i.e., log(p(k|x))
    Args:
      x should be batched i.e., [batch, m] array
    '''
    log_probs = torch.stack(
                  [sampler.log_prob(x) for sampler in self.mv_normal_samplers],
                  dim=1)
    log_posteriors = log_probs + self.log_priors
    return log_posteriors - torch.logsumexp(log_posteriors, dim=-1)[...,None]

  def log_prob(self, x):
    '''
    Returns the log probability of a sample x.
      i.e., log(p(x)) = log(\sum_k p(x|k)p(k))
    '''
    log_probs = torch.stack(
                  [sampler.log_prob(x) for sampler in self.mv_normal_samplers],
                  dim=1)
    log_posteriors = log_probs + self.log_priors
    return torch.logsumexp(log_posteriors, dim=-1)

class GaussianMixtureSampler(Sampler):
  def __init__(self, config, locs_list, covs_list=None,
               component_priors_list=None, mixture_priors=None):
    '''
    Generates data from a mixture of gaussian distribution for k mixtures
      each component has locs as the mean, cov as the covariances and component_priors
      as priors for each component of each mixture.
    Args:
      config: configuration object
      locs_list: list of [n_k,m] numpy arrays with locs_list[k] is the
            the locs array of the k-th mixture with each locs_list[k][r]) representing
            the mean for the r-th component of the k-th mixture.
      covs_list: list of [n_k,m,m] numpy arrays of covariances where covs_list[k][r]
            is the covariance matrix for the r-th component of the k-th mixture.
            if convs is None we assume identity covariances.
      component_priors_list: list of [n_k,] numpy arrays with
            component_priors_list[k][r] is the prior of the r-th component of
            r-th mixture.
      mixture_priors: [k,] numpy array which priors[r] is used as the relative
              priors for k-th mixture.

    '''
    super(GaussianMixtureSampler, self).__init__()
    self.num_mixtures = len(locs_list)
    if mixture_priors is None:
      mixture_priors = torch.tensor(np.array([1./self.num_mixtures]*self.num_mixtures,
                                    dtype=np.float32))
    if covs_list is None:
      covs_list = [None]*self.num_mixtures
    if component_priors_list is None:
      component_priors_list = [None]*self.num_mixtures
    assert len(locs_list) == len(covs_list) \
        == len(component_priors_list) == len(mixture_priors) \
        , 'mixtures shapes mismatch'

    self.mixture_priors = mixture_priors
    self.log_mixture_priors = torch.log(self.mixture_priors)
    self.mvn_samplers = []
    for locs, covs, component_priors in zip(locs_list, covs_list, component_priors_list):
      self.mvn_samplers.append(MultivariateNormalSampler(config,
                                                         locs,covs, component_priors))
  def sample(self):
    '''samples a class from the mixture,
       then samples a point from that.
    '''
    k =  torch.multinomial(self.mixture_priors, 1)
    x, component = self.mvn_samplers[int(k[0])].sample()
    return x,k

  def log_posterior(self, x):
    '''
    Returns the log probability of a sample x for each class.
      i.e., p(k|x)
    '''
    log_probs = torch.stack(
                  [sampler.log_prob(x) for sampler in self.mvn_samplers],
                  dim=1)
    log_posteriors = log_probs + self.log_mixture_priors
    return log_posteriors - torch.logsumexp(log_posteriors, dim=-1)[...,None]

  def log_prob(self,x):
    '''
    Returns the log probability of a sample x.
      i.e., log(p(x)) = log(\sum_k p(x|k)p(k))
    '''
    log_probs = torch.stack(
                  [sampler.log_prob(x) for sampler in self.mvn_samplers],
                  dim=1)
    log_posteriors = log_probs + self.log_mixture_priors
    return torch.logsumexp(log_posteriors, dim=-1)

def get_random_covs_isotropic(num, dim):
  """This produces almost dim*I covariances
  """
  identity_covs = torch.eye(dim)[None].repeat(num,1,1)
  covs = torch.randn(num, dim, dim, dtype=torch.float32)
  covs = torch.matmul(covs, covs.transpose(2,1))
  covs = covs + identity_covs*1e-15
  return covs

def get_random_covs_eigen(num, dim):
  """This is borrowed from scikit-learn make_spd_matrix code
  """
  covs = []
  m = torch.distributions.half_cauchy.HalfCauchy(torch.ones(dim))
  for i in range(num): #pytorch does not have batch svd
    x = torch.randn(dim,dim)
    #U,s,V = torch.svd(torch.matmul(x.t(), x))
    U,s,V = torch.svd(x)
    #V is transposed in torch.svd compared to numpy.linalg.svd
    #D = torch.diag(torch.rand(dim)+1e-12)
    D = torch.diag(m.sample()+1e-12)
    covs.append(V.mm(D.mm(V.t())))
  return torch.stack(covs)

def get_random_covs_lkj(num, dim):
  """
  sample covariances based on LKJ prior using pyro
  Args:
    df: degree of freedom
  see:
    https://github.com/pyro-ppl/pyro/pull/1746/files
  """
  covs = []
  for i in range(num):
    theta = pyro.sample("theta", dist.HalfCauchy(2*torch.ones(dim)))
    eta = torch.ones(1)
    L_omega = pyro.sample("L_omega", dist.LKJCorrCholesky(dim, eta))
    L_Omega = torch.mm(torch.diag(theta.sqrt()), L_omega)
    covs.append(torch.mm(L_Omega, L_Omega.t()))
    #print(covs[-1])
  return torch.stack(covs)

def get_random_covs(num, dim):
  #TODO: make it a class and use config
  return get_random_covs_lkj(num, dim)

def get_random_mvn_sampler(config):
  locs = torch.randn(config.num_classes, config.dim, dtype=torch.float32)
  if 'locs_scale' in config:
    locs = locs * config.locs_scale
  covs = None
  priors = None
  if 'cov_type' in config:
    covs = get_random_covs(config.num_classes, config.dim)
  sampler = MultivariateNormalSampler(config, locs, covs, priors)
  return sampler

def get_random_mixture_sampler(config):
  locs_list = []
  covs_list = []
  min_num_components = 2 if not 'min_num_components' in config else config.min_num_components
  max_num_components = 5 if not 'max_num_components' in config else config.max_num_components

  assert min_num_components < max_num_components, 'min num components should be less than max' \
      'num components'

  for i in range(config.num_classes):
    n_components = torch.randint(min_num_components,
                                 max_num_components,
                                 [1])
    print(i, n_components)
    locs = torch.randn(n_components, config.dim, dtype=torch.float32)
    if 'locs_scale' in config:
      locs = locs * config.locs_scale
    locs_list.append(locs)

    identity_covs = torch.eye(config.dim)[None].repeat(n_components,1,1)
    if 'cov_type' in config:
      if config.cov_type == 'random':
        covs = get_random_covs(n_components, config.dim)
      elif config.cov_type == 'identity':
        covs = identity_covs
      else:
        raise NotImplementedError('cov_type: {} is not supported'.format(config.cov_type))
      covs_list.append(covs)
    else:
      covs_list.append(identity_covs)

  sampler = GaussianMixtureSampler(config, locs_list, covs_list=covs_list)
  return sampler

def get_sampler(config, sampler_type):
  if sampler_type == 'random_mixture':
    return get_random_mixture_sampler(config)
  elif sampler_type == 'random_mvn':
    return get_random_mvn_sampler(config)
  else:
    raise NotImplementedError('Sampler not implemented yet')


