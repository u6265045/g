"""Performs t-SNE on dataset"""

import argparse
import logging
import os

import numpy as np
import torch
from torch.autograd import Variable
import torch.nn.functional as F
import utils
import net
import dataset as data_loader
import json
from easydict import EasyDict
from time import time
from tsnecuda import TSNE

import matplotlib as mpl
mpl.use('pdf')
import matplotlib.pyplot as plt

plt.rc('font', family='serif', serif='Times', size=22)
plt.rc('text', usetex=True)
plt.rc('xtick', labelsize=15)
plt.rc('ytick', labelsize=15)
plt.rc('axes', labelsize=15)
width = (3.487 + 0.3)*1.3
height = width * 1.00/ 1.618 + 1.5

parser = argparse.ArgumentParser()
parser.add_argument('--model_dir', default='experiments/base_model', help="Directory containing config.json")


def tsne(dataloader, config, sampler, save_path):
  """tsne the model on `num_eval` batches.

  Args:
      dataloader: (DataLoader) a torch.utils.data.DataLoader object that fetches data
      config: (config) hyperparameters
      sampler: (Sampler) dataset sampler
      save_path: where to save visualization file
  """

  # summary for current eval loop
  all_labels = []
  all_batches = []
  # compute metrics over the dataset
  num_eval = 2
  #if 'num_eval' in config:
  #  num_eval = config.num_eval
  i = 0
  for data_batch, labels_batch in dataloader:
    all_batches.append(data_batch)
    all_labels.append(labels_batch)
    i += 1
    if num_eval is not None and i == num_eval:
      break
  X = torch.cat(all_batches).data.numpy()
  Y = torch.cat(all_labels)[:,0].data.numpy()
  print('Computing t-SNE...')
  X_embedded = TSNE(n_components=2, perplexity=30, learning_rate=600.0).fit_transform(X)
  print(' - done.')
  print('Saving figure')
  N = config.num_classes
  tag = Y.astype(np.int64)
  cmap = plt.cm.jet
  cmaplist = [cmap(i) for i in range(cmap.N)]
  cmap = cmap.from_list('Custom cmap',cmaplist, cmap.N)
  bounds = np.linspace(0,N,N+1)
  norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
  colors = Y/10.0
  fig, ax = plt.subplots()
  fig.subplots_adjust(left=.15, bottom=.2, right=.95, top=.9)
  scat = ax.scatter(X_embedded[:,0], X_embedded[:,1],c=tag, cmap=cmap, norm=norm, alpha=0.3)
  cb = plt.colorbar(scat, spacing='proportional',ticks=bounds)
  cb.set_label('Classes')
  cb.set_alpha(1)
  cb.draw_all()
  fig.set_size_inches(width, height)
  fig.savefig('tsne_p30_lr_600_alpha_0.5_lkj.pdf')
  plt.close(fig)


if __name__ == '__main__':
  """
      Evaluate the model on the test set.
  """
  # Load the parameters
  args = parser.parse_args()
  json_path = os.path.join(args.model_dir, 'config.json')
  assert os.path.isfile(json_path), "No json configuration file found at {}".format(json_path)
  with open(json_path, 'r') as f:
    config = EasyDict(json.load(f))
  config.locs_scale = 0.5
  # use GPU if available
  config.cuda = torch.cuda.is_available()     # use GPU is available

  # Set the random seed for reproducible experiments
  seed = 1357 if 'seed' not in config else config.seed
  torch.manual_seed(1357)
  if config.cuda: torch.cuda.manual_seed(1357)
  utils.fix_rng_seed(seed)

  # Get the logger
  utils.set_logger(os.path.join(args.model_dir, 'evaluate.log'))

  # Create the input data pipeline
  logging.info("Creating the dataset...")

  # fetch dataloaders
  test_dl, sampler = data_loader.fetch_dataloader(config)

  logging.info("- done.")

  # viz file
  save_path = os.path.join(args.model_dir, "input_tsne.pdf")
  tsne(test_dl, config, sampler, save_path)
