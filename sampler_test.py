import numpy as np
from easydict import EasyDict
import sampler as data_sampler
import torch

import matplotlib as mpl
mpl.use('pdf')
import matplotlib.pyplot as plt

plt.rc('font', family='serif', serif='Times', size=22)
plt.rc('text', usetex=True)
plt.rc('xtick', labelsize=15)
plt.rc('ytick', labelsize=15)
plt.rc('axes', labelsize=15)
width = (3.487 + 0.3)*1.3
height = width * 1.00/ 1.618 + 1.5

def draw2d_normal():
  locs = torch.tensor(np.array([[0,0], [3,3]], dtype=np.float32))
  priors = torch.tensor(np.array([0.3, 0.7], dtype=np.float32))
  covs = torch.tensor(np.array([[[1,0.5], [0.5,1]],[[2,-0.5],[-0.5,1]]], dtype=np.float32))

  sampler = data_sampler.MultivariateNormalSampler(None, locs, covs, priors)
  xs = []
  ks = []
  num_samples = 600
  for i in range(num_samples):
    x,k = sampler.sample()
    xs.append(x)
    ks.append(k)
  all_xs = torch.stack(xs)
  all_ks = torch.stack(ks)
  X = all_xs[:,0].data.numpy()
  Y = all_xs[:,1].data.numpy()
  colors = all_ks[:,0].data.numpy().astype(np.float32)
  num0 = len(colors[colors==0])/ len(colors)
  num1 = len(colors[colors==1])/ len(colors)
  print('First class: {}, second class: {}'.format(num0, num1))
  colors = (colors + 0.5)/2
  fig, ax = plt.subplots()
  fig.subplots_adjust(left=.15, bottom=.2, right=.95, top=.9)
  ax.scatter(X,Y,c=colors, cmap='viridis', alpha=0.3)
  fig.set_size_inches(width, height)
  fig.savefig('normal.pdf')
  plt.close(fig)

def draw2d_random_mixture():
  config = EasyDict(num_classes=3, dim=2)
  sampler = data_sampler.get_random_mixture_sampler(config)
  xs = []
  ks = []
  num_samples = 600
  for i in range(num_samples):
    x,k = sampler.sample()
    xs.append(x)
    ks.append(k)
  all_xs = torch.stack(xs)
  all_ks = torch.stack(ks)
  X = all_xs[:,0].data.numpy()
  Y = all_xs[:,1].data.numpy()
  colors = all_ks[:,0].data.numpy().astype(np.float32)
  num0 = len(colors[colors==0])/ len(colors)
  num1 = len(colors[colors==1])/ len(colors)
  num2 = len(colors[colors==2])/ len(colors)
  print('First class: {}, second class: {}, third class: {}'.format(num0, num1,num2))
  colors = (colors*0.3) + 0.2
  fig, ax = plt.subplots()
  fig.subplots_adjust(left=.15, bottom=.2, right=.95, top=.9)
  ax.scatter(X,Y,c=colors, cmap='viridis', alpha=0.3)
  fig.set_size_inches(width, height)
  fig.savefig('mog_random.pdf')
  plt.close(fig)

def draw2d_mixture():
  config = EasyDict(num_classes=3, dim=2)

  locs_list = [np.array([[0,0], [5,5]], dtype=np.float32),
               np.array([[-2,-2], [3,-3], [5,0]], dtype=np.float32),
               np.array([[-2, 5], [3, -5], [3,3]], dtype=np.float32)]
  locs_list = [torch.tensor(x) for x in locs_list]
  mixture_priors = torch.tensor(np.array([0.2, 0.6, 0.2], dtype=np.float32))
  covs = None #np.array([[[1,0.5], [0.5,1]],[[2,-0.5],[-0.5,1]]], dtype=np.float32)
  sampler = data_sampler.GaussianMixtureSampler(config, locs_list, mixture_priors=mixture_priors)

  xs = []
  ks = []
  num_samples = 600
  for i in range(num_samples):
    x,k = sampler.sample()
    xs.append(x)
    ks.append(k)
  all_xs = torch.stack(xs)
  all_ks = torch.stack(ks)
  X = all_xs[:,0].data.numpy()
  Y = all_xs[:,1].data.numpy()
  colors = all_ks[:,0].data.numpy().astype(np.float32)
  num0 = len(colors[colors==0])/ len(colors)
  num1 = len(colors[colors==1])/ len(colors)
  num2 = len(colors[colors==2])/ len(colors)
  print('First class: {}, second class: {}, third class: {}'.format(num0, num1,num2))
  colors = (colors*0.3) + 0.2
  fig, ax = plt.subplots()
  fig.subplots_adjust(left=.15, bottom=.2, right=.95, top=.9)
  ax.scatter(X,Y,c=colors, cmap='viridis', alpha=0.3)
  fig.set_size_inches(width, height)
  fig.savefig('mog.pdf')
  plt.close(fig)
  t = torch.tensor(np.array([[1.5, 1.5]], dtype=np.float32))
  print('Posterior and probability of {}: {}, {}'.format(t,
    sampler.posterior(t), sampler.prob(t)))

def draw2d_random_mixture_random_covariances():
  config = EasyDict(num_classes=3, dim=2, cov_type='random', locs_scale=10.)
  sampler = data_sampler.get_random_mixture_sampler(config)
  xs = []
  ks = []
  num_samples = 20000
  for i in range(num_samples):
    x,k = sampler.sample()
    xs.append(x)
    ks.append(k)
  all_xs = torch.stack(xs)
  all_ks = torch.stack(ks)
  X = all_xs[:,0].data.numpy()
  Y = all_xs[:,1].data.numpy()
  colors = all_ks[:,0].data.numpy().astype(np.float32)
  num0 = len(colors[colors==0])/ len(colors)
  num1 = len(colors[colors==1])/ len(colors)
  num2 = len(colors[colors==2])/ len(colors)
  print('First class: {}, second class: {}, third class: {}'.format(num0, num1,num2))
  colors = (colors*0.3) + 0.2
  fig, ax = plt.subplots()
  fig.subplots_adjust(left=.15, bottom=.2, right=.95, top=.9)
  ax.scatter(X,Y,c=colors, cmap='viridis', alpha=0.3)
  fig.set_size_inches(width, height)
  fig.savefig('mog_random_cov_random.pdf')
  plt.close(fig)


if __name__ == '__main__':
  draw2d_normal()
  draw2d_random_mixture()
  draw2d_mixture()
  draw2d_random_mixture_random_covariances()
