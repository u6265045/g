"""Train the model"""

import argparse
import logging
import os

import numpy as np
import torch
import torch.optim as optim
from torch.autograd import Variable
from tqdm import tqdm

import utils
import net
import dataset as data_loader
from evaluate import evaluate
from easydict import EasyDict
import json

parser = argparse.ArgumentParser()
parser.add_argument('--model_dir', default='experiments/base', help="Directory containing config.json")
parser.add_argument('--restore_file', default=None,
                    help="Optional, name of the file in --model_dir containing weights \
                          to reload before training")  # 'best' or 'train'


def train(model, optimizer, loss_fn, dataloader, metrics, config):
    """Train the model on `num_steps` batches

    Args:
        model: (torch.nn.Module) the neural network
        optimizer: (torch.optim) optimizer for parameters of model
        loss_fn: a function that takes batch_output and batch_labels and computes the loss for the batch
        dataloader: (DataLoader) a torch.utils.data.DataLoader object that fetches training data
        metrics: (dict) a dictionary of functions that compute a metric using the output and labels of each batch
        config: (config) hyperparameters
        num_steps: (int) number of batches to train on, each of size config.batch_size
    """

    # set model to training mode
    model.train()

    # summary for current training loop and a running average object for loss
    summ = []
    loss_avg = utils.RunningAverage()

    # Use tqdm for progress bar
    with tqdm(total=len(dataloader)) as t:
        for i, (train_batch, labels_batch) in enumerate(dataloader):
            # move to GPU if available
            if config.cuda:
                train_batch, labels_batch = train_batch.cuda(async=True), labels_batch.cuda(async=True)
            # convert to torch Variables
            train_batch, labels_batch = Variable(train_batch), Variable(labels_batch)

            # compute model output and loss (outputs are logits)
            output_batch = model(train_batch)
            loss = loss_fn(output_batch, labels_batch[:,0])

            # clear previous gradients, compute gradients of all variables wrt loss
            optimizer.zero_grad()
            loss.backward()

            # performs updates using calculated gradients
            optimizer.step()
            loss_val = loss.data.cpu().numpy()

            # Evaluate summaries only once in a while
            if (i+1) % config.train.save_summary_steps == 0:
                # extract data from torch Variable, move to cpu, convert to numpy arrays
                output_batch = output_batch.data.cpu().numpy()
                labels_batch = labels_batch.data.cpu().numpy()

                # compute all metrics on this batch
                summary_batch = {metric:metrics[metric](output_batch, labels_batch)
                                 for metric in metrics}
                #loss_val = loss.data.cpu().numpy()
                summary_batch['loss'] = loss_val
                summ.append(summary_batch)

            # update the average loss
            loss_avg.update(loss_val)

            t.set_postfix(loss='{:05.3f}'.format(loss_avg()))
            t.update()

    # compute mean of all metrics in summary
    metrics_mean = {metric:np.mean([x[metric] for x in summ]) for metric in summ[0]}
    metrics_string = " ; ".join("{}: {:05.3f}".format(k, v) for k, v in metrics_mean.items())
    logging.info("- Train metrics: " + metrics_string)


def train_and_evaluate(model, train_dataloader, val_dataloader, optimizer, loss_fn, metrics, config, model_dir,
                       sampler, restore_file=None):
    """Train the model and evaluate every epoch.

    Args:
        model: (torch.nn.Module) the neural network
        train_dataloader: (DataLoader) a torch.utils.data.DataLoader object that fetches training data
        val_dataloader: (DataLoader) a torch.utils.data.DataLoader object that fetches validation data
        optimizer: (torch.optim) optimizer for parameters of model
        loss_fn: a function that takes batch_output and batch_labels and computes the loss for the batch
        metrics: (dict) a dictionary of functions that compute a metric using the output and labels of each batch
        config: (config) hyperparameters
        model_dir: (string) directory containing config, weights and log
        sampler: (Sampler) the sampler which used for sampling data.
        restore_file: (string) optional- name of file to restore from (without its extension .pth.tar)
    """
    # reload weights from restore_file if specified
    if restore_file is not None:
        restore_path = os.path.join(args.model_dir, args.restore_file + '.pth.tar')
        logging.info("Restoring parameters from {}".format(restore_path))
        utils.load_checkpoint(restore_path, model, optimizer)

    best_mean_abs_prob_diff = 2.0

    for epoch in range(config.train.num_epochs):
        # Run one epoch
        logging.info("Epoch {}/{}".format(epoch + 1, config.train.num_epochs))

        # compute number of batches in one epoch (one full pass over the training set)
        train(model, optimizer, loss_fn, train_dataloader, metrics, config)

        # Evaluate for one epoch on validation set
        val_metrics = evaluate(model, loss_fn, val_dataloader, metrics, config, sampler)

        val_mean_abs_prob_diff = val_metrics['mean_abs_prob_diff']
        is_best = val_mean_abs_prob_diff <= best_mean_abs_prob_diff

        # Save weights
        utils.save_checkpoint({'epoch': epoch + 1,
                               'state_dict': model.state_dict(),
                               'optim_dict' : optimizer.state_dict()},
                               is_best=is_best,
                               checkpoint=model_dir)

        # If best_eval, best_save_path
        val_metrics.update({'epoch':epoch+1})
        if is_best:
            logging.info("- Found new best MAPD")
            best_mean_abs_prob_diff = val_mean_abs_prob_diff

            # Save best val metrics in a json file in the model directory
            best_json_path = os.path.join(model_dir, "metrics_val_best_weights.json")
            utils.save_dict_to_json(val_metrics, best_json_path)

        # Save latest val metrics in a json file in the model directory
        last_json_path = os.path.join(model_dir, "metrics_val_last_weights.json")
        utils.save_dict_to_json(val_metrics, last_json_path)

if __name__ == '__main__':

    # Load the parameters from json file
    args = parser.parse_args()
    json_path = os.path.join(args.model_dir, 'config.json')
    assert os.path.isfile(json_path), "No json configuration file found at {}".format(json_path)
    with open(json_path, 'r') as f:
      config = EasyDict(json.load(f))

    # use GPU if available
    config.cuda = torch.cuda.is_available()

    # Set the random seed for reproducible experiments
    seed = 1357 if 'seed' not in config else config.seed
    torch.manual_seed(seed)
    if config.cuda: torch.cuda.manual_seed(seed)
    utils.fix_rng_seed(seed)

    # Set the logger
    utils.set_logger(os.path.join(args.model_dir, 'train.log'))

    # Create the input data pipeline
    logging.info("Loading the dataset...")

    # fetch dataloaders
    dl, sampler = data_loader.fetch_dataloader(config)

    logging.info("- done.")

    # Define the model and optimizer
    model = net.get_model(config)

    model = model.cuda() if config.cuda else model
    optimizer = optim.Adam(model.parameters(),
                           lr=config.train.learning_rate,
                           weight_decay=config.train.weight_decay)

    # fetch loss function and metrics
    loss_fn = net.get_loss_fn(config)
    metrics = net.get_metrics_fn(config)

    # Train the model
    logging.info("Starting training for {} epoch(s)".format(config.train.num_epochs))
    # train_dl, val_dl are the same
    train_and_evaluate(model, dl, dl, optimizer, loss_fn, metrics, config, args.model_dir,
                       sampler, args.restore_file)
